﻿using Microsoft.EntityFrameworkCore;

namespace WeSell.Database.CategoryManagement
{
  public partial class CategoryManagementContext : DbContext
  {
    public CategoryManagementContext()
    {
    }

    public CategoryManagementContext(DbContextOptions<CategoryManagementContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Brand> Brand { get; set; }
    public virtual DbSet<Category> Category { get; set; }


    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<Brand>(entity =>
      {
        entity.ToTable("Brand", "CategoryManagement");

        entity.Property(e => e.Id).HasColumnName("ID");

        entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

        entity.Property(e => e.Name)
                  .IsRequired()
                  .HasMaxLength(50);

        entity.HasOne(d => d.Category)
                  .WithMany(p => p.Brand)
                  .HasForeignKey(d => d.CategoryId)
                  .OnDelete(DeleteBehavior.ClientSetNull)
                  .HasConstraintName("FK_Brand_Category");
      });

      modelBuilder.Entity<Category>(entity =>
      {
        entity.ToTable("Category", "CategoryManagement");

        entity.Property(e => e.Id).HasColumnName("ID");

        entity.Property(e => e.Name)
                  .IsRequired()
                  .HasMaxLength(50);
      });
    }
  }
}
