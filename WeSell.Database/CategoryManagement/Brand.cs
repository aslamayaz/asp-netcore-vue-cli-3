﻿namespace WeSell.Database.CategoryManagement
{
  public partial class Brand
  {
    public int CategoryId { get; set; }
    public string Name { get; set; }
    public int Id { get; set; }
    public string Icon { get; set; }
    public Category Category { get; set; }
  }
}
