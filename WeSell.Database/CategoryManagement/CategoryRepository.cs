﻿using Microsoft.EntityFrameworkCore;

namespace WeSell.Database.CategoryManagement
{
  public class CategoryRepository : Repository.Repository, ICategoryRepository
  {
    protected override DbContext Context
    {
      get;
    }

    public CategoryRepository(CategoryManagementContext categoryManagementContext)
    {
      Context = categoryManagementContext;
    }
  }
}