﻿using System.Collections.Generic;

namespace WeSell.Database.CategoryManagement
{
  public partial class Category
  {
    public Category()
    {
      Brand = new HashSet<Brand>();
    }

    public int Id { get; set; }
    public string Name { get; set; }
    public string Icon { get; set; }

    public ICollection<Brand> Brand { get; set; }
  }
}
