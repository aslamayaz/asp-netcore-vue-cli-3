﻿namespace WeSell.Database.ProductModels
{
  public class HardDisk
  {
    public string Size { get; set; }
    public string Type { get; set; }
    public string Format { get; set; }

    public HardDisk(ProductManagement.HardDisk hardDisk)
    {
      Size = hardDisk.Size;
      Type = hardDisk.Type;
      Format = hardDisk.Format;
    }
  }
}