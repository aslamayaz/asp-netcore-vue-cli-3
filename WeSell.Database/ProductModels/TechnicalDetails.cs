﻿namespace WeSell.Database.ProductModels
{
  public class TechnicalDetails
  {

    public TechnicalDetails(ProductManagement.Display display, ProductManagement.HardDisk hardDisk, ProductManagement.Processor processor, ProductManagement.Ram ram, ProductManagement.Specification specification)
    {
      Display = new Display( display);
      HardDisk = new HardDisk(hardDisk);
      Processor = new Processor(processor);
      Ram = new Ram(ram);
      HarstelleNumber = specification.HarstelleNumber;
      Eannumber = specification.Eannumber;
    }

    public string HarstelleNumber { get; set; }
    public string Eannumber { get; set; }
    public Display Display { get; set; }
    public HardDisk HardDisk { get; set; }
   
    public Processor Processor { get; set; }
    public Ram Ram { get; set; }
  }
}