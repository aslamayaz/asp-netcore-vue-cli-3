﻿namespace WeSell.Database.ProductModels
{
  public class Display
  {
    public string Size { get; set; }
    public string Resolution { get; set; }
    public string Art { get; set; }
    public bool Led { get; set; }
    public string Hdtv { get; set; }

    public Display(ProductManagement.Display display)
    {
      Size = display.Size;
      Resolution = display.Resolution;
      Art = display.Art;
      Led = display.Led;
      Hdtv = display.Hdtv;
    }
  }
}