﻿namespace WeSell.Database.ProductModels
{
  public class Processor
  {
    public string Turboboost { get; set; }
    public string Cache { get; set; }
    public string Type { get; set; }

    public Processor(ProductManagement.Processor processor)
    {
      Turboboost = processor.Turboboost;
      Cache = processor.Cache;
      Type = processor.Type;
    }
  }
}