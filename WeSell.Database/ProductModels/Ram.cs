﻿namespace WeSell.Database.ProductModels
{
  public class Ram
  {
     public string Size { get; set; }
    public string Technology { get; set; }

    public Ram(ProductManagement.Ram ram)
    {
      Size = ram.Size;
      Technology = ram.Technology;
    }
  }
}
