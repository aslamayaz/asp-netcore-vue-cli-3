﻿using System.Collections.Generic;
using System.Linq;

namespace WeSell.Database.ProductModels
{
  public class ProductInfo
  {
    public string Category { get; set; }
    public string Brand { get; set; }
    public int? ArticleNumber { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public int? Price { get; set; }
    public string Thumbnail { get; set; }
    public double? Rating { get; set; }
    public double? Discount { get; set; }
    public IEnumerable<string> ImageUrls { get; set; }

    public TechnicalDetails TechnicalDetails { get; set; }

    public void GetBasicInfo(ProductManagement.Product product, CategoryManagement.Brand brand)
    {
      Category = brand.Category.Name;
      Brand = brand.Name;
      Name = product.Name;
      ArticleNumber = product.ArticleNumber;
      Description = product.Description;
      Price = product.Price;
      Thumbnail = product.Thumbnail;
      Rating = product.Rating;
      Discount = product.Discount;
    }

    public void GetFullInfo(ProductManagement.Product product, CategoryManagement.Brand brand)
    {
      Category = brand.Category.Name;
      Brand = brand.Name;
      Name = product.Name;
      ArticleNumber = product.ArticleNumber;
      Description = product.Description;
      Price = product.Price;
      Thumbnail = product.Thumbnail;
      Rating = product.Rating;
      Discount = product.Discount;

      ImageUrls = getUrls(product.Images);

      TechnicalDetails = new TechnicalDetails(
        product.Display.First(),
        product.HardDisk.First(), 
        product.Processor.First(), 
        product.Ram.First(),
        product.Specification.First()
        );
    }

    private IEnumerable<string> getUrls(ICollection<ProductManagement.Images> images)
    {
      var imageUrls = new List<string>();
      foreach (var image in images)
      {
        imageUrls.Add(image.ImageUrl);
      }
      return  imageUrls;
    }
  }
}