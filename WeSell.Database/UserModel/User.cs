﻿namespace WeSell.Database.UserModel
{
  public class User
  {
    private string email;
    public string Gender { get; set; }

    public string Firstname { get; set; }
    public string Lastname { get; set; }
    public string Streetno { get; set; }
    public string Houseno { get; set; }
    public string Postcode { get; set; }
    public string City { get; set; }
    public string Country { get; set; }
    public string Telephone { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public string Email { get; set; }
  }
}