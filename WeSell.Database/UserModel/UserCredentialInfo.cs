﻿namespace WeSell.Database.UserModel
{
  public class UserCredentialInfo
  {
    public string Username { get; set; }
    public string Password { get; set; }
  }
}
