﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace WeSell.Database.Repository
{

  public interface IRepository
  {
    IQueryable<TEntity> FindAll<TEntity>(Expression<Func<TEntity, bool>> predicate, int? skip = null, int? take = null) where TEntity : class;

    IQueryable<TEntity> FindAll<TEntity>() where TEntity : class;

    IQueryable<TEntity> Find<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class;


    void Add<TEntity>(TEntity entity) where TEntity : class;
    void AddRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class;


    void Remove<TEntity>(TEntity entity) where TEntity : class;
    void RemoveRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class;

    int Save();

  }
}
