﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace WeSell.Database.Repository
{
  public abstract class Repository : IRepository
  {
    protected abstract DbContext Context { get; }

    public IQueryable<TEntity> FindAll<TEntity>(Expression<Func<TEntity, bool>> predicate, int? skip = null, int? take = null) where TEntity : class
    {
      var query = Context.Set<TEntity>().Where(predicate);

      if (skip.HasValue)
      {
        query = query.Skip(skip.Value);
      }

      if (take.HasValue)
      {
        query = query.Take(take.Value);
      }

      return query;
    }

    public IQueryable<TEntity> FindAll<TEntity>() where TEntity : class
    {
      return Context.Set<TEntity>();
    }

    public IQueryable<TEntity> Find<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
    {
      return Context.Set<TEntity>().Where(predicate);
    }

    public void Add<TEntity>(TEntity entity) where TEntity : class
    {
      Context.Set<TEntity>().Add(entity);
    }

    public void AddRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
    {
      Context.Set<TEntity>().AddRange(entities);
    }

    public void Remove<TEntity>(TEntity entity) where TEntity : class
    {
      Context.Set<TEntity>().Remove(entity);
    }

    public void RemoveRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
    {
      Context.Set<TEntity>().RemoveRange(entities);
    }

    public int Save()
    {
      try
      {
        var result = Context.SaveChanges();
        return result;
      }
      catch (Exception exception)
      {
        Console.WriteLine(exception);
        throw;
      }
    }
  }
}