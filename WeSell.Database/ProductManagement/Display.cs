﻿using System;
using System.Collections.Generic;

namespace WeSell.Database.ProductManagement
{
    public partial class Display
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Size { get; set; }
        public string Resolution { get; set; }
        public string Art { get; set; }
        public bool Led { get; set; }
        public string Hdtv { get; set; }

        public Product Product { get; set; }
    }
}
