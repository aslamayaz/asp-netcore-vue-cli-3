﻿using System;
using System.Collections.Generic;

namespace WeSell.Database.ProductManagement
{
    public partial class HardDisk
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Size { get; set; }
        public string Type { get; set; }
        public string Format { get; set; }

        public Product Product { get; set; }
    }
}
