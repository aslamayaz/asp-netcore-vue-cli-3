﻿using System;
using System.Collections.Generic;

namespace WeSell.Database.ProductManagement
{
    public partial class Images
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string ImageUrl { get; set; }

        public Product Product { get; set; }
    }
}
