﻿using System.Collections.Generic;
using WeSell.Database.CategoryManagement;

namespace WeSell.Database.ProductManagement
{
  public partial class Product
  {
    public Product()
    {
      Display = new HashSet<Display>();
      HardDisk = new HashSet<HardDisk>();
      Images = new HashSet<Images>();
      Processor = new HashSet<Processor>();
      Ram = new HashSet<Ram>();
      Specification = new HashSet<Specification>();
    }

    public int Id { get; set; }
    public int BrandId { get; set; }
    public int CategoryId { get; set; }
    public int? ArticleNumber { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public int? Price { get; set; }
    public string Thumbnail { get; set; }
    public double? Rating { get; set; }
    public double? Discount { get; set; }
    public ICollection<Display> Display { get; set; }
    public ICollection<HardDisk> HardDisk { get; set; }
    public ICollection<Images> Images { get; set; }
    public ICollection<Processor> Processor { get; set; }
    public ICollection<Ram> Ram { get; set; }
    public ICollection<Specification> Specification { get; set; }
  }
}
