﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WeSell.Database.ProductManagement
{
    public partial class ProductManagementContext : DbContext
    {
        public ProductManagementContext()
        {
        }

        public ProductManagementContext(DbContextOptions<ProductManagementContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Display> Display { get; set; }
        public virtual DbSet<HardDisk> HardDisk { get; set; }
        public virtual DbSet<Images> Images { get; set; }
        public virtual DbSet<Processor> Processor { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Ram> Ram { get; set; }
        public virtual DbSet<Specification> Specification { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Display>(entity =>
            {
                entity.ToTable("Display", "ProductManagement");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Hdtv).HasColumnName("HDTV");

                entity.Property(e => e.Led).HasColumnName("LED");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.Resolution).IsRequired();

                entity.Property(e => e.Size).IsRequired();

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Display)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Display_Product");
            });

            modelBuilder.Entity<HardDisk>(entity =>
            {
                entity.ToTable("HardDisk", "ProductManagement");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Format).IsRequired();

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.Size).IsRequired();

                entity.Property(e => e.Type).IsRequired();

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.HardDisk)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_HardDisk_Product");
            });

            modelBuilder.Entity<Images>(entity =>
            {
                entity.ToTable("Images", "ProductManagement");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ImageUrl).IsRequired();

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Images)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Images_Product");
            });

            modelBuilder.Entity<Processor>(entity =>
            {
                entity.ToTable("Processor", "ProductManagement");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Cache).IsRequired();

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.Type).IsRequired();

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Processor)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Processor_Product");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("Product", "ProductManagement");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BrandId).HasColumnName("BrandID");
            });

            modelBuilder.Entity<Ram>(entity =>
            {
                entity.ToTable("RAM", "ProductManagement");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.Size).IsRequired();

                entity.Property(e => e.Technology).IsRequired();

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Ram)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RAM_Product");
            });

            modelBuilder.Entity<Specification>(entity =>
            {
                entity.ToTable("Specification", "ProductManagement");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Eannumber)
                    .IsRequired()
                    .HasColumnName("EANNumber")
                    .HasMaxLength(50);

                entity.Property(e => e.HarstelleNumber)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ProductId).HasColumnName("ProductID");

                entity.Property(e => e.Ram).HasColumnName("RAM");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Specification)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Specification_Product");
            });
        }
    }
}
