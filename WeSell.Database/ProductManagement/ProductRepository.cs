﻿using Microsoft.EntityFrameworkCore;
using WeSell.Database.CategoryManagement;
using WeSell.Database.Repository;

namespace WeSell.Database.ProductManagement
{
  public class ProductRepository : Repository.Repository, IProductRepository
  {
    protected override DbContext Context
    {
      get;
    }

    public ProductRepository(ProductManagementContext productManagementContext)
    {
      Context = productManagementContext;
    }
  }
}