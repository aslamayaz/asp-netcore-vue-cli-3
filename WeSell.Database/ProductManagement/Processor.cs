﻿using System;
using System.Collections.Generic;

namespace WeSell.Database.ProductManagement
{
    public partial class Processor
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Turboboost { get; set; }
        public string Cache { get; set; }
        public string Type { get; set; }

        public Product Product { get; set; }
    }
}
