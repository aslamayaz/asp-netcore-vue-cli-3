﻿using System;
using System.Collections.Generic;

namespace WeSell.Database.ProductManagement
{
    public partial class Specification
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string HarstelleNumber { get; set; }
        public string Eannumber { get; set; }
        public bool Processor { get; set; }
        public bool Display { get; set; }
        public bool Ram { get; set; }
        public bool HardDisk { get; set; }

        public Product Product { get; set; }
    }
}
