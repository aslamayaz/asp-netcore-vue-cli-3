﻿using System;
using System.Collections.Generic;

namespace WeSell.Database.ProductManagement
{
    public partial class Ram
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Size { get; set; }
        public string Technology { get; set; }

        public Product Product { get; set; }
    }
}
