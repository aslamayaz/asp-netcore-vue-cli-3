﻿using System;
using System.Collections.Generic;

namespace WeSell.Database.UserManagement
{
    public partial class UserCredentials
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public UserInfo User { get; set; }
    }
}
