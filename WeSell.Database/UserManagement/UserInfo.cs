﻿using System;
using System.Collections.Generic;

namespace WeSell.Database.UserManagement
{
    public partial class UserInfo
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public DateTime? CreatedOn { get; set; }

        public UserCredentials UserCredentials { get; set; }
    }
}
