﻿using Microsoft.EntityFrameworkCore;

namespace WeSell.Database.UserManagement
{
  public class UserRepository : Repository.Repository, IUserRepository
  {

    protected override DbContext Context
    {
      get;
    }

    public UserRepository(UserManagementContext userManagementContext)
    {
      Context = userManagementContext;
    }
  }
}