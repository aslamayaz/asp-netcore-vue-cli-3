﻿using Microsoft.EntityFrameworkCore;

namespace WeSell.Database.UserManagement
{
  public partial class UserManagementContext : DbContext
  {
    public UserManagementContext()
    {
    }

    public UserManagementContext(DbContextOptions<UserManagementContext> options)
        : base(options)
    {
    }

    public virtual DbSet<UserCredentials> UserCredentials { get; set; }
    public virtual DbSet<UserInfo> UserInfo { get; set; }


    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<UserCredentials>(entity =>
      {
        entity.HasKey(e => e.UserId);

        entity.ToTable("UserCredentials", "UserManagement");

        entity.Property(e => e.UserId)
                  .HasColumnName("UserID")
                  .ValueGeneratedNever();

        entity.Property(e => e.Password)
                  .IsRequired()
                  .HasColumnName("password");

        entity.Property(e => e.Username)
                  .IsRequired()
                  .HasMaxLength(50);

        entity.HasOne(d => d.User)
                  .WithOne(p => p.UserCredentials)
                  .HasForeignKey<UserCredentials>(d => d.UserId)
                  .OnDelete(DeleteBehavior.ClientSetNull)
                  .HasConstraintName("FK_UserCredentials_UserInfo");
      });

      modelBuilder.Entity<UserInfo>(entity =>
      {
        entity.ToTable("UserInfo", "UserManagement");

        entity.Property(e => e.Id).HasColumnName("ID");

        entity.Property(e => e.City)
                  .IsRequired()
                  .HasMaxLength(50);

        entity.Property(e => e.CreatedOn).HasColumnType("datetime");

        entity.Property(e => e.Email)
                  .IsRequired()
                  .HasMaxLength(50);

        entity.Property(e => e.FirstName)
                  .IsRequired()
                  .HasMaxLength(50);

        entity.Property(e => e.HouseNumber)
                  .IsRequired()
                  .HasMaxLength(50);

        entity.Property(e => e.LastName)
                  .IsRequired()
                  .HasMaxLength(50);

        entity.Property(e => e.PostCode)
                  .IsRequired()
                  .HasMaxLength(50);

        entity.Property(e => e.Street)
                  .IsRequired()
                  .HasMaxLength(50);

        entity.Property(e => e.Telephone)
                  .IsRequired()
                  .HasMaxLength(50);
      });
    }
  }
}
