using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using vue.Email;
using VueCliMiddleware;
using WeSell.Database.CategoryManagement;
using WeSell.Database.ProductManagement;
using WeSell.Database.UserManagement;

namespace vue
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

      services.AddMvc()
        .AddJsonOptions(
          options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
        );

      // In production, the React files will be served from this directory
      services.AddSpaStaticFiles(configuration =>
      {
        configuration.RootPath = "ClientApp/build";
      });

      var connection = @"Server=(local)\SQLEXPRESS;Database=WeSell;Trusted_Connection=True;User ID=ayazaslam27;Password=Badman21892*";
      services.AddDbContext<CategoryManagementContext>(options => options.UseSqlServer(connection));
      services.AddDbContext<ProductManagementContext>(options => options.UseSqlServer(connection));
      services.AddDbContext<UserManagementContext>(options => options.UseSqlServer(connection));


      //Repositories
      services.AddTransient<ICategoryRepository, CategoryRepository>();
      services.AddTransient<IProductRepository, ProductRepository>();
      services.AddTransient<IUserRepository, UserRepository>();


      //From asppsettings.json
      services.Configure<Settings>(Configuration.GetSection("EmailSettings"));

      //Services
      services.AddTransient<IEmailService, EmailService>();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseExceptionHandler(" / Error");
      }

      app.UseStaticFiles();
      app.UseSpaStaticFiles();

      app.UseMvc(routes =>
      {
        routes.MapRoute(
                  name: "default",
                  template: "{controller}/{action=Index}/{id?}");
      });

      app.UseSpa(spa =>
      {
        spa.Options.SourcePath = "ClientApp";

        if (env.IsDevelopment())
        {
          spa.UseVueCli(npmScript: "serve");
          spa.UseProxyToSpaDevelopmentServer("http://localhost:8080");
        }
      });
    }
  }
}
