using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WeSell.Database.CategoryManagement;

namespace vue.Controllers
{
  [Route("api/[controller]")]
  public class CategoryController : Controller
  {
    private readonly ICategoryRepository _categoryRepository;
    public CategoryController(ICategoryRepository categoryRepository)
    {
      _categoryRepository = categoryRepository;
    }

    [HttpGet("[action]")]
    public JsonResult GetCategories()
    {
      var result = _categoryRepository.FindAll<Category>().Include(x => x.Brand);
      return Json(result);
    }
  }
}
