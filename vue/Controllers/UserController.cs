﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using vue.Email;
using WeSell.Database.UserManagement;
using WeSell.Database.UserModel;

namespace vue.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class UserController : Controller
  {
    private readonly IUserRepository _userRepository;
    private readonly IEmailService _emailService;

    public UserController(IUserRepository userRepository, IEmailService emailService)
    {
      _userRepository = userRepository;
      _emailService = emailService;
    }

    [HttpPost("[action]")]
    public JsonResult Register([FromBody]User user)
    {
      var userEntity = new UserInfo
      {
        FirstName = user.Firstname,
        LastName = user.Lastname,
        Street = user.Streetno,
        HouseNumber = user.Houseno,
        Telephone = user.Telephone,
        PostCode = user.Postcode,
        Email = user.Email,
        City = user.City,
        CreatedOn = DateTime.UtcNow
      };

      var credentials = new UserCredentials();
      credentials.Username = user.Username;
      var passwordHasher = new PasswordHasher<UserCredentials>();
      credentials.Password = passwordHasher.HashPassword(credentials, user.Password);

      userEntity.UserCredentials = credentials;

      _userRepository.Add<UserInfo>(userEntity);
      var result = _userRepository.Save();
      return Json(result);
    }

    [HttpPost("[action]")]
    public JsonResult Login([FromBody]UserCredentialInfo userCredential)
    {

      var user = _userRepository.Find<UserCredentials>(x => x.Username == userCredential.Username).First();

      var passwordHasher = new PasswordHasher<UserCredentials>();
      var isPasswordEqual = passwordHasher.VerifyHashedPassword(user, user.Password, userCredential.Password);
      return Json(isPasswordEqual);
    }

    [HttpPost("[action]")]
    public JsonResult SendEmail()
    {
      _emailService.SendEmail("ayazaslam27@gmail.com", "We are testing", "Hello We are selling stuff");

      return Json("Successfully sent");
    }
  }
}