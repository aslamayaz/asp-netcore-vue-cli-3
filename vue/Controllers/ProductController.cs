using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using WeSell.Database.CategoryManagement;
using WeSell.Database.ProductManagement;
using ProductInfo = WeSell.Database.ProductModels.ProductInfo;

namespace vue.Controllers
{
  [Route("api/[controller]")]
  public class ProductController : Controller
  {
    private readonly IProductRepository _productRepository;
    private readonly ICategoryRepository _categoryRepository;
    public ProductController(IProductRepository productRepository, ICategoryRepository categoryRepository)
    {
      _productRepository = productRepository;
      _categoryRepository = categoryRepository;
    }

    [HttpGet("[action]")]
    public JsonResult GetProducts([FromQuery]string category, [FromQuery]string make, [FromQuery]string limit, [FromQuery]string pageNo)
    {
      var skip = (int.Parse(pageNo) - 1) * int.Parse(limit);
      var take = int.Parse(limit);

      Product[] products;

      if (!string.IsNullOrEmpty(make) && !string.IsNullOrEmpty(category))
      {
        var brandEntity = _categoryRepository.Find<Brand>(x => x.Name.Contains(make))
          .Include(x=>x.Category).First();
        products = _productRepository.FindAll<Product>(x => x.BrandId == brandEntity.Id && x.CategoryId == brandEntity.Category.Id, skip, take).ToArray();
      }
      else
      {
        var categoryEntity = _categoryRepository.Find<Category>(x => x.Name.Contains(category)).First();

        products = _productRepository.FindAll<Product>(x => x.CategoryId == categoryEntity.Id, skip, take).ToArray();
      }

      var productsInfo = new List<ProductInfo>();
      foreach (var product in products)
      {
        var brand = _categoryRepository.Find<Brand>(x=>x.Id == product.BrandId)
          .Include(x=>x.Category).First();
        var productInfo = new ProductInfo();
        productInfo.GetBasicInfo(product, brand);
        productsInfo.Add(productInfo);
      }
     
      return Json(productsInfo);
    }

    [HttpGet("[action]")]
    public JsonResult GetTotalProducts([FromQuery]string category, [FromQuery]string brand)
    {
      if (!string.IsNullOrEmpty(brand) && !string.IsNullOrEmpty(category))
      {
        var brandEntity = _categoryRepository.Find<Brand>(x => x.Name.Contains(brand))
          .Include(x => x.Category).First();
        var count = _productRepository.FindAll<Product>(x => x.BrandId == brandEntity.Id && x.CategoryId == brandEntity.Category.Id).Count();
        return Json(count);
      }
      else
      {
        var categoryEntity = _categoryRepository.Find<Category>(x => x.Name.Contains(category)).First();

        var count = _productRepository.FindAll<Product>(x => x.CategoryId == categoryEntity.Id).Count();
        return Json(count);
      }
     
    }

    [HttpGet("[action]")]
    public JsonResult GetTopProducts()
    {
      var topProducts = _productRepository.FindAll<Product>().Where(x => x.Rating > 3.5).ToArray();
      var productsInfo = new List<ProductInfo>();
      foreach (var product in topProducts)
      {
        var brandEntity = _categoryRepository.Find<Brand>(x => x.Id == product.BrandId)
          .Include(x=>x.Category).First();
        var productInfo = new ProductInfo();
        productInfo.GetBasicInfo(product, brandEntity);
        productsInfo.Add(productInfo);
      }

      return Json(productsInfo);
    }


    [HttpGet("[action]")]
    public JsonResult GetProduct([FromQuery]string brand, [FromQuery]int articleNumber)
    {
      if (!string.IsNullOrEmpty(brand))
      {
        var brandEntity = _categoryRepository.Find<Brand>(x => x.Name.Contains(brand))
          .Include(x=>x.Category).First();

        var product = _productRepository.FindAll<Product>(x => x.BrandId == brandEntity.Id && x.ArticleNumber == articleNumber)
          .Include(x => x.Specification)
          .Include(x => x.Images)
          .Include(x => x.Display)
          .Include(x => x.Ram)
          .Include(x => x.Processor)
          .Include(x => x.HardDisk)
          .First();

        var productInfo = new ProductInfo();
        productInfo.GetFullInfo(product, brandEntity);
        return Json(productInfo);
      }

      return Json(null);
    }
  }
}
