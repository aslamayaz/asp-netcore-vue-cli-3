﻿var getters = {
  getCategories: function(state) {
    return state.categories;
  },

  getItems: function(state) {
    return state.items;
  },

  getMenu: function(state) {
    return state.menu;
  },

  getCartItems: function(state) {
    return state.cartItems;
  },

  getTopProducts: function(state) {
    return state.topProducts;
  }
};

export default {
  getCategories: getters.getCategories,
  getItems: getters.getItems,
  getMenu: getters.getMenu,
  getCartItems: getters.getCartItems,
  getTopProducts: getters.getTopProducts
};
