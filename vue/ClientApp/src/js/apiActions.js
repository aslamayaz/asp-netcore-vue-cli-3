﻿import caller from "./caller";
var url = {
  categoriesUrl: "/api/Category/GetCategories",
  productsUrl: "/api/Product/GetProducts",
  topProductsUrl: "/api/Product/GetTopProducts",
  totalProductsUrl: "/api/Product/GetTotalProducts",
  productUrl: "/api/Product/GetProduct",
  newUserRegisterUrl: "/api/User/Register",
  loginUrl: "/api/User/Login"
};

var getCategories = function(callback) {
  caller.get(url.categoriesUrl, null, callback);
};

var getProducts = function(params, callback) {
  caller.get(url.productsUrl, params, callback);
};

var getTotalProducts = function(params, callback) {
  caller.get(url.totalProductsUrl, params, callback);
};

var getTopProducts = function(callback) {
  caller.get(url.topProductsUrl, null, callback);
};

var getProduct = function(params, callback) {
  caller.get(url.productUrl, params, callback);
};

var register = function(params, callback) {
  caller.post(url.newUserRegisterUrl, params, callback);
};

var login = function(params, callback) {
  caller.post(url.loginUrl, params, callback);
};

export default {
  getCategories: getCategories,
  getProducts: getProducts,
  getTotalProducts: getTotalProducts,
  getTopProducts: getTopProducts,
  getProduct: getProduct,
  register: register,
  login: login
};
