﻿const actions = {
  setCategories({ commit }, categories) {
    commit("SET_CATEGORIES", categories);
  },

  setItems({ commit }, items) {
    commit("SET_ITEMS", items);
  },

  setMenu({ commit }, menu) {
    commit("SET_MENU", menu);
  },

  setCartItem({ commit }, item) {
    commit("SET_CARTITEM", item);
  },

  setTopProducts({ commit }, topProducts) {
    commit("SET_TOPPRODUCTS", topProducts);
  }
};

export default {
  setCategories: actions.setCategories,
  setItems: actions.setItems,
  setMenu: actions.setMenu,
  setCartItem: actions.setCartItem,
  setTopProducts: actions.setTopProducts
};
