export const itemMixin = {
  data() {
    return {};
  },
  computed: {
    arrowStyle() {
      if (this.showChildren) {
        return `transform: translateY(-50%) rotate(90deg) `;
      } else if (!this.showChildren) {
        return `transform: translateY(-50%) rotate(0deg) `;
      }
    }
  }
};

export const eventMixin = {
  data() {
    return {
      initiateMobileVersion: false,
      orientationMode: null,
      showCart: false,
      openSideMenu: false
    };
  },
  mounted() {
    this.update();

    this.$nextTick(() => {
      window.addEventListener("resize", this.update);
    });

    this.$on("showCartEvent", val => {
      this.showCart = val;
    });

    this.$on("toggleSideMenu", val => {
      this.openSideMenu = val;
    });
  },
  methods: {
    update() {
      if (window.outerWidth < 1000) {
        this.initiateMobileVersion = true;
        this.showCart = false;
      } else {
        this.initiateMobileVersion = false;
      }
    }
  },
  beforeDestroy() {
    window.removeEventListener("resize", this.update);
  }
};
