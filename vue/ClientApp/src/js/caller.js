﻿import axios from "axios";

function get(url, params, callback) {
  axios
    .get(url, { params })
    .then(function(response) {
      callback(response.data);
    })
    .catch(function(error) {
      console.log(error);
    })
    .then(function() {});
}

function post(url, params, callback) {
  axios
    .post(url, params)
    .then(function(response) {
      callback(response.data);
    })
    .catch(function(error) {
      console.log(error);
    });
}

export default { get, post };
