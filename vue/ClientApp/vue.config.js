﻿// vue.config.js
module.exports = {
    devServer: {
        proxy: {
            '^/api': {
              target: 'https://localhost:44393/',
              ws: true,
              changeOrigin: true
            },
        }
    }
};