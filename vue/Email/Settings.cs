﻿namespace vue.Email
{
  public class Settings
  {
    public string PrimaryDomain { get; set; }

    public int PrimaryPort { get; set; }

    public string SecondayDomain { get; set; }

    public int SecondaryPort { get; set; }

    public string Username { get; set; }

    public string Password { get; set; }
  }
}
