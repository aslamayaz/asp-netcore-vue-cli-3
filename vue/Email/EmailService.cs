﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace vue.Email
{
  public class EmailService : IEmailService
  {
    private readonly Settings _settings;
    public EmailService(IOptions<Settings> emailSettings)
    {
      _settings = emailSettings.Value;
    }

    public async Task SendEmail(string email, string subject, string htmlMessage)
    {
      try
      {
        var mail = new MailMessage()
        {
          From = new MailAddress(_settings.Username, "Ayaz Aslam")
        };

        mail.To.Add(new MailAddress(email));

        mail.Subject = "Personal Management System - " + subject;
        mail.Body = htmlMessage;
        mail.IsBodyHtml = true;
        mail.Priority = MailPriority.High;

        using (var smtp = new SmtpClient(_settings.PrimaryDomain, _settings.PrimaryPort))
        {
          smtp.Credentials = new NetworkCredential(_settings.Username, _settings.Password);
          smtp.EnableSsl = true;
          await smtp.SendMailAsync(mail);
        }
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
      }

    }
  }
}
